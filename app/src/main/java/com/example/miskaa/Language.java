package com.example.miskaa;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Language {

    @SerializedName("iso639_1")
    @Expose
    private String iso639_1;

    @SerializedName("iso639_2")
    @Expose
    private String iso639_2;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("nativeName")
    @Expose
    private String nativeName;

    public String getIso639_1() {
        return iso639_1;
    }

    public String getIso639_2() {
        return iso639_2;
    }

    public String getName() {
        return name;
    }

    public String getNativeName() {
        return nativeName;
    }
}
