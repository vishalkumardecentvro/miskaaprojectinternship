package com.example.miskaa.Room;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.miskaa.Repository.BorderRepository;

@Database(entities = {CountryInformationEntity.class, CountryBordersEntity.class, CountryLanguageEntity.class},version = 1)
public abstract class CountryDatabase extends RoomDatabase {

    private static CountryDatabase instance; // we have created this variable because we want this class to be singleton, so that no other instance can be created
    public abstract InformationDao informationDao();
    public abstract BorderDao borderDao();
    public abstract LanguageDao languageDao();

    public static synchronized CountryDatabase getInstance(Context context){
        if(instance == null){

            instance = Room.databaseBuilder(context.getApplicationContext(),CountryDatabase.class,"Country Database")
                    .fallbackToDestructiveMigrationFrom()
                    .addCallback(roomcallBack)
                    .build();
        }
        return instance;

    }

    public static RoomDatabase.Callback roomcallBack = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            super.onCreate(db);
        }
    };


}
