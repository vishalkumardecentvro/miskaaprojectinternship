package com.example.miskaa.Room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface LanguageDao {

    @Insert
    void insertLanguages (CountryLanguageEntity countryLanguageEntity);

    @Query("DELETE FROM language")
    void deleteAllNotes();

    @Query("SELECT * FROM language")
    LiveData<List<CountryLanguageEntity>> getAllLanguagesInfo();
}
