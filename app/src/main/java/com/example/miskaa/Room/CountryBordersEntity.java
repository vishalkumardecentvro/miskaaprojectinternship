package com.example.miskaa.Room;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "borders")
public class CountryBordersEntity {

    String borderName;
    String countryName;

    public CountryBordersEntity(String borderName, String countryName) {
        this.borderName = borderName;
        this.countryName = countryName;
    }

    public String getBorderName() {
        return borderName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setId(int id) {
        this.id = id;
    }

    @PrimaryKey(autoGenerate = true)
    private int id;

    public int getId() {
        return id;
    }
}
