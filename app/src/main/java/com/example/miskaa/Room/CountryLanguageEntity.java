package com.example.miskaa.Room;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "language")
public class CountryLanguageEntity {

    private String iso639_1;
    private String iso639_2;
    private String name;
    private String nativeName;
    private String countryName;

    public CountryLanguageEntity(String iso639_1, String iso639_2, String name, String nativeName,String countryName) {
        this.iso639_1 = iso639_1;
        this.iso639_2 = iso639_2;
        this.name = name;
        this.nativeName = nativeName;
        this.countryName = countryName;
    }

    @PrimaryKey(autoGenerate = true)
    private int id;

    public void setId(int id) {
        this.id = id;
    }

    public String getIso639_1() {
        return iso639_1;
    }

    public String getIso639_2() {
        return iso639_2;
    }

    public String getName() {
        return name;
    }

    public String getNativeName() {
        return nativeName;
    }

    public String getCountryName() {
        return countryName;
    }

    public int getId() {
        return id;
    }
}
