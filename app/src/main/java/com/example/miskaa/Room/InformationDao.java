package com.example.miskaa.Room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface InformationDao {

    @Insert
    void insertInformation (CountryInformationEntity countryInformation);

    @Query("DELETE FROM generalInformation")
    void deleteAllNotes();

    @Query("SELECT * FROM generalInformation")
    LiveData<List<CountryInformationEntity>> getAllInformation();
}
