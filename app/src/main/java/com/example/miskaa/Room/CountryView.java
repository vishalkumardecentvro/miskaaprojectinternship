package com.example.miskaa.Room;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.miskaa.Repository.BorderRepository;
import com.example.miskaa.Repository.CountryInformationRepository;
import com.example.miskaa.Repository.LanguageRepository;

import java.util.List;

public class CountryView extends AndroidViewModel {

    private CountryInformationRepository repository;
    private BorderRepository borderRepository;
    private LanguageRepository languageRepository;
    private LiveData<List<CountryInformationEntity>> allInformation;
    private LiveData<List<CountryBordersEntity>> allBorders;
    private LiveData<List<CountryLanguageEntity>> allLanguages;

    public CountryView(@NonNull Application application) {
        super(application);
        repository = new CountryInformationRepository(application);
        borderRepository = new BorderRepository(application);
        languageRepository = new LanguageRepository(application);

        allInformation = repository.getAllInformation();
        allBorders = borderRepository.getAllInformation();
        allLanguages = languageRepository.getAllInformation();
    }

    public void insert(CountryInformationEntity countryInformationEntity){
        repository.insertInfo(countryInformationEntity);
    }

    public void insertBorders(CountryBordersEntity countryBordersEntity){
        borderRepository.insertBorder(countryBordersEntity);
    }

    public void insertLanguage(CountryLanguageEntity countryLanguageEntity){
        languageRepository.insertLanguage(countryLanguageEntity);
    }

    public LiveData<List<CountryInformationEntity>> getAllInformation(){
        return allInformation;
    }

    public LiveData<List<CountryLanguageEntity>> getAllLanguages(){
        return allLanguages;
    }

    public LiveData<List<CountryBordersEntity>> getAllBorders(){
        return allBorders;
    }

    public void DeleteGenralInfo ( ){
        repository.deleteAllInformation();
    }

    public void DeleteBorderlInfo ( ){
        borderRepository.deleteAllInformation();
    }

    public void DeleteLanguage ( ){
        languageRepository.deleteAllInformation();
    }

}
