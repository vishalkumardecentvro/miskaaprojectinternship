package com.example.miskaa.Room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface BorderDao {

    @Insert
    void insertBorderInfo (CountryBordersEntity countryBorders);

    @Query("DELETE FROM borders")
    void deleteAllNotes();

    @Query("SELECT * FROM borders")
    LiveData<List<CountryBordersEntity>> getAllBorders();
}


