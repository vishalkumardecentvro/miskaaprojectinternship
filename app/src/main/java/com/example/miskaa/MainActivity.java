package com.example.miskaa;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.miskaa.Adapters.CountryAdapter;
import com.example.miskaa.Room.CountryBordersEntity;
import com.example.miskaa.Room.CountryInformationEntity;
import com.example.miskaa.Room.CountryLanguageEntity;
import com.example.miskaa.Room.CountryView;
import com.wessam.library.NetworkChecker;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private RecyclerView countryRecyclerView;
    private CountryAdapter countryAdapter;
    private OfflineAdapter offlineAdapter;
    Context context;
    LinearLayoutManager layoutManager;
    CountryView countryView;
    List<String> borders = new ArrayList<>();
    List<Language> language = new ArrayList<>();
    private ImageView offlineImage;
    private Button offlineButton;
    List<CountryInformationEntity> offlineGeneralINformation = new ArrayList<>();
    List<CountryBordersEntity> offlineBorders = new ArrayList<>();
    List<CountryLanguageEntity> offlineLanguages = new ArrayList<>();
    private Button deleteButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = MainActivity.this;

        countryRecyclerView = findViewById(R.id.countriesRecyclerView);
        countryView = ViewModelProviders.of(this).get(CountryView.class);
        offlineImage = findViewById(R.id.oflineImage);
        offlineButton = findViewById(R.id.offlineButton);
        deleteButton = findViewById(R.id.deleteDataButton);

        if (NetworkChecker.isNetworkConnected(MainActivity.this))
            getData();
        else {
            offlineImage.setVisibility(View.VISIBLE);
            offlineButton.setVisibility(View.VISIBLE);
        }

        offlineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchGeneralInformation();
                fetchLanguages();
                fetchBorders();
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                countryView.DeleteGenralInfo();
                countryView.DeleteBorderlInfo();
                countryView.DeleteLanguage();
                deleteButton.setVisibility(View.GONE);
                offlineAdapter.notifyDataSetChanged();
                deleteButton.setVisibility(View.GONE);
                //finish();

            }
        });

    }

    private void fetchBorders() {

        countryView = ViewModelProviders.of(this).get(CountryView.class);
        countryView.getAllBorders().observe(MainActivity.this, new Observer<List<CountryBordersEntity>>() {
            @Override
            public void onChanged(List<CountryBordersEntity> countryBorderEntities) {
                offlineBorders = countryBorderEntities;


            }
        });
    }

    private void fetchLanguages() {

        countryView = ViewModelProviders.of(this).get(CountryView.class);
        countryView.getAllLanguages().observe(MainActivity.this, new Observer<List<CountryLanguageEntity>>() {
            @Override
            public void onChanged(List<CountryLanguageEntity> countryLanguageEntities) {
                offlineLanguages = countryLanguageEntities;

            }
        });
    }

    private void fetchGeneralInformation() {

        countryView = ViewModelProviders.of(this).get(CountryView.class);
        countryView.getAllInformation().observe(MainActivity.this, new Observer<List<CountryInformationEntity>>() {
            @Override
            public void onChanged(List<CountryInformationEntity> countryInformationEntities) {
                offlineGeneralINformation = countryInformationEntities;
                offlineImage.setVisibility(View.GONE);
                offlineButton.setVisibility(View.GONE);

                offlineAdapter = new OfflineAdapter(countryInformationEntities, offlineBorders, offlineLanguages, context);
                layoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
                countryRecyclerView.setLayoutManager(layoutManager);
                countryRecyclerView.setAdapter(offlineAdapter);
                deleteButton.setVisibility(View.VISIBLE);

            }
        });


    }

    private void getData() {

        Call<List<CountryData>> data = Connection.getCountryInterface().getCountries();
        data.enqueue(new Callback<List<CountryData>>() {
            @Override
            public void onResponse(Call<List<CountryData>> call, Response<List<CountryData>> response) {
                List<CountryData> countryData = response.body();
                countryAdapter = new CountryAdapter(countryData, context);
                layoutManager = new LinearLayoutManager(MainActivity.this, LinearLayoutManager.VERTICAL, false);
                countryRecyclerView.setLayoutManager(layoutManager);
                countryRecyclerView.setAdapter(countryAdapter);

                for (int i = 0; i < countryData.size(); i++) {
                    String countryName = countryData.get(i).getCountryName();
                    String capital = countryData.get(i).getCapital();
                    String flag = countryData.get(i).getFlag();
                    String region = countryData.get(i).getRegion();
                    String subRegion = countryData.get(i).getSubregion();
                    String population = countryData.get(i).getPopulation();
                    borders = countryData.get(i).getBorders();
                    language = countryData.get(i).getLanguages();

                    Log.i("name", countryData.get(i).getCountryName());
                    Log.i("name", countryData.get(i).getFlag());
                    Log.i("name", countryData.get(i).getCapital());
                    Log.i("name", countryData.get(i).getLanguages().get(0).getNativeName());

                    CountryInformationEntity countryInformationEntity = new CountryInformationEntity(countryName, capital, flag, region, subRegion, population);
                    countryView.insert(countryInformationEntity);

                    for (int j = 0; j < borders.size(); j++) {
                        CountryBordersEntity countryBordersEntity = new CountryBordersEntity(borders.get(j), countryName);
                        countryView.insertBorders(countryBordersEntity);
                    }
                    Log.i("size", String.valueOf(language.size()));
                    for (int k = 0; k < language.size(); k++) {

                        CountryLanguageEntity countryLanguageEntity = new CountryLanguageEntity(language.get(k).getIso639_1(), language.get(k).getIso639_2(),
                                language.get(k).getName(), language.get(k).getNativeName(), countryName);
                        countryView.insertLanguage(countryLanguageEntity);

                    }


                }


            }

            @Override
            public void onFailure(Call<List<CountryData>> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Problem in loadin data",Toast.LENGTH_SHORT).show();

            }
        });
    }


}