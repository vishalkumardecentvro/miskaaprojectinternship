package com.example.miskaa.Repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.miskaa.Room.CountryDatabase;
import com.example.miskaa.Room.CountryInformationEntity;
import com.example.miskaa.Room.InformationDao;

import java.util.List;

public class CountryInformationRepository {

    private InformationDao informationDao;
    private LiveData<List<CountryInformationEntity>> allInformation;

    public CountryInformationRepository(Application application) {
        CountryDatabase countryDatabase = CountryDatabase.getInstance(application);
        this.informationDao = countryDatabase.informationDao();
        this.allInformation = informationDao.getAllInformation();
    }

    public void insertInfo(CountryInformationEntity countryInformation){
        new InsertAsyncTask(informationDao).execute(countryInformation);
    }

    public void deleteAllInformation(){

        new DeleteAllAsyncNoteTask(informationDao).execute();
    }

    public LiveData<List<CountryInformationEntity>> getAllInformation() {
        return allInformation;
    }

    private static class InsertAsyncTask extends AsyncTask<CountryInformationEntity,Void,Void> {

        private InformationDao informationDao;

        private InsertAsyncTask(InformationDao informationDao){  // constructor
            this.informationDao = informationDao;
        }

        @Override
        protected Void doInBackground(CountryInformationEntity... countryInformations) {
            informationDao.insertInformation(countryInformations[0]);
            return null;
        }
    }

    private static class DeleteAllAsyncNoteTask extends AsyncTask<Void,Void,Void>{

        private InformationDao informationDao;

        public DeleteAllAsyncNoteTask(InformationDao informationDao) {
            this.informationDao = informationDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            informationDao.deleteAllNotes();
            return null;
        }
    }
}
