package com.example.miskaa.Repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.miskaa.Room.BorderDao;
import com.example.miskaa.Room.CountryBordersEntity;
import com.example.miskaa.Room.CountryDatabase;
import com.example.miskaa.Room.CountryLanguageEntity;
import com.example.miskaa.Room.LanguageDao;

import java.util.List;

public class BorderRepository {

    private BorderDao borderDao;
    private LiveData<List<CountryBordersEntity>> allBorders;

    public BorderRepository(Application application) {
        CountryDatabase countryDatabase = CountryDatabase.getInstance(application);
        this.borderDao = countryDatabase.borderDao();
        this.allBorders = borderDao.getAllBorders();
    }

    public void insertBorder(CountryBordersEntity countryBordersEntity){
        new InsertAsyncTask(borderDao).execute(countryBordersEntity);
    }

    public void deleteAllInformation(){

        new DeleteAllAsyncNoteTask(borderDao).execute();
    }

    public LiveData<List<CountryBordersEntity>> getAllInformation() {
        return allBorders;
    }

    private static class InsertAsyncTask extends AsyncTask<CountryBordersEntity,Void,Void> {

        private BorderDao borderDao;

        private InsertAsyncTask(BorderDao borderDao){  // constructor
            this.borderDao = borderDao;
        }

        @Override
        protected Void doInBackground(CountryBordersEntity... countryBordersEntities) {
            borderDao.insertBorderInfo(countryBordersEntities[0]);
            return null;
        }
    }

    private static class DeleteAllAsyncNoteTask extends AsyncTask<Void,Void,Void>{

        private BorderDao borderDao;

        public DeleteAllAsyncNoteTask(BorderDao borderDao) {
            this.borderDao = borderDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            borderDao.deleteAllNotes();
            return null;
        }
    }


}
