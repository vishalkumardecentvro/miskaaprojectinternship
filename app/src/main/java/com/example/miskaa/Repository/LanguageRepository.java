package com.example.miskaa.Repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.example.miskaa.Room.CountryDatabase;
import com.example.miskaa.Room.CountryInformationEntity;
import com.example.miskaa.Room.CountryLanguageEntity;
import com.example.miskaa.Room.InformationDao;
import com.example.miskaa.Room.LanguageDao;

import java.util.List;

public class LanguageRepository {

    private LanguageDao languageDao;
    private LiveData<List<CountryLanguageEntity>> allLanguages;

    public LanguageRepository(Application application) {
        CountryDatabase countryDatabase = CountryDatabase.getInstance(application);
        this.languageDao = countryDatabase.languageDao();
        this.allLanguages = languageDao.getAllLanguagesInfo();
    }

    public void insertLanguage(CountryLanguageEntity countryLanguageEntity){
        new InsertAsyncTask(languageDao).execute(countryLanguageEntity);
    }

    public void deleteAllInformation(){

        new DeleteAllAsyncNoteTask(languageDao).execute();
    }

    public LiveData<List<CountryLanguageEntity>> getAllInformation() {
        return allLanguages;
    }

    private static class InsertAsyncTask extends AsyncTask<CountryLanguageEntity,Void,Void> {

        private LanguageDao languageDao;

        private InsertAsyncTask(LanguageDao languageDao){  // constructor
            this.languageDao = languageDao;
        }

        @Override
        protected Void doInBackground(CountryLanguageEntity... countryLanguageEntities) {
            languageDao.insertLanguages(countryLanguageEntities[0]);
            return null;
        }
    }

    private static class DeleteAllAsyncNoteTask extends AsyncTask<Void,Void,Void>{

        private LanguageDao languageDao;

        public DeleteAllAsyncNoteTask(LanguageDao languageDao) {
            this.languageDao = languageDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            languageDao.deleteAllNotes();
            return null;
        }
    }
}
