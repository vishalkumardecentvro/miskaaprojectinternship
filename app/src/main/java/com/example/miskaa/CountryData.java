package com.example.miskaa;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CountryData {

    @SerializedName("name")
    @Expose
    private String countryName;

    @SerializedName("capital")
    @Expose
    private String capital;

    @SerializedName("flag")
    @Expose
    private String flag;

    @SerializedName("region")
    @Expose
    private String region;

    @SerializedName("subregion")
    @Expose
    private String subregion;

    @SerializedName("population")
    @Expose
    private String population;

    @SerializedName("borders")
    @Expose
    private List<String> borders;

    @SerializedName("languages")
    @Expose
    private List<Language> languages;

    public String getCountryName() {
        return countryName;
    }

    public String getCapital() {
        return capital;
    }

    public String getFlag() {
        return flag;
    }

    public String getRegion() {
        return region;
    }

    public String getSubregion() {
        return subregion;
    }

    public String getPopulation() {
        return population;
    }

    public List<String> getBorders() {
        return borders;
    }

    public List<Language> getLanguages() {
        return languages;
    }
}
