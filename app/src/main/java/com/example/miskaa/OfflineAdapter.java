package com.example.miskaa;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.miskaa.Room.CountryBordersEntity;
import com.example.miskaa.Room.CountryInformationEntity;
import com.example.miskaa.Room.CountryLanguageEntity;

import java.util.ArrayList;
import java.util.List;

public class OfflineAdapter extends RecyclerView.Adapter<OfflineAdapter.ViewHolder>{

    List<CountryInformationEntity> countryDataList;
    List<CountryBordersEntity> borders;
    List<CountryLanguageEntity> languages;
    Context context;
    @NonNull
    @Override
    public OfflineAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View countryView = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_card,parent,false);
        OfflineAdapter.ViewHolder viewHolder = new OfflineAdapter.ViewHolder(countryView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull OfflineAdapter.ViewHolder holder, int position) {

        CountryInformationEntity currentData = countryDataList.get(position);
        holder.countryName.setText(currentData.getCountryName());
        holder.capital.setText(currentData.getCapital());
        holder.region.setText(currentData.getRegion());
        holder.subregion.setText(currentData.getSubregion());
        holder.population.setText(currentData.getPopulation());



//        String flagUrl = currentData.getFlag();
//        SvgImageLoader.fetchSvg(context,flagUrl,holder.flag);

//        if(currentBorder.getCountryName().equals(currentData.getCountryName()))
//
//        List<String> border = currentBorder.getBorderName();
//        ArrayAdapter<String> borderAdapter = new ArrayAdapter<String>(context,R.layout.support_simple_spinner_dropdown_item,border);
//        borderAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
//        holder.border.setAdapter(borderAdapter);
//
//        ArrayList<String> languageArrayList = new ArrayList<String>();
//        languageArrayList.add(languages.get(0).getName());
//        languageArrayList.add(languages.get(0).getNativeName());
//        languageArrayList.add(languages.get(0).getIso639_1());
//        languageArrayList.add(languages.get(0).getIso639_2());
//
//        for(Language lang : countryLanguageList){
//            languageArrayList.add(lang.getName());
//            languageArrayList.add(lang.getNativeName());
//            languageArrayList.add(lang.getIso639_1());
//            languageArrayList.add(lang.getIso639_2());
//        }
//
//        ArrayAdapter<String> languageAdapter = new ArrayAdapter<String>(context,R.layout.support_simple_spinner_dropdown_item,languageArrayList);
//        languageAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
//        holder.languages.setAdapter(languageAdapter);
    }

    @Override
    public int getItemCount() {
        return countryDataList.size();
    }

    public OfflineAdapter(List<CountryInformationEntity> data,List<CountryBordersEntity> borders,List<CountryLanguageEntity> languages ,Context context) {
        this.countryDataList = data;
        this.context = context;
        this.borders = borders;
        this.languages = languages;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView countryName,capital,region,subregion,population;
        ImageView flag;
        Spinner border,languages;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            countryName = itemView.findViewById(R.id.countryNameText);
            capital = itemView.findViewById(R.id.capitalText);
            region = itemView.findViewById(R.id.regionText);
            subregion = itemView.findViewById(R.id.subregionText);
            population = itemView.findViewById(R.id.populationText);

            flag = itemView.findViewById(R.id.countryFlagImage);
            border = itemView.findViewById(R.id.borderSpinner);
            languages = itemView.findViewById(R.id.languagesSpinner);

        }
    }

}
